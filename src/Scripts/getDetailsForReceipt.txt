USE ANIMAL_SHELTER
GO

CREATE PROCEDURE getDetailsForReceipt @receiptID INTEGER
	AS
		BEGIN
			SELECT 
			phoneNumber,
			CustomerName,
			PetName,
			T1.ReceiptID,
			WeekNumber
			FROM 
			Customer T4
			INNER JOIN 
			(
			Pet T3
			INNER JOIN (
			Receipt T1
			INNER JOIN 
			Booking T2
			ON 
			T1.ReceiptID = T2.ReceiptID
			) ON T3.PetID = T2.PetID
			) ON T4.phoneNumber = T3.cust_tlf WHERE T1.ReceiptID = @receiptID


		END